import discord as dc
from lib.const import BOT, TOKEN
from lib.logging import log
import os


@BOT.event
async def on_ready():
    log("Bot logged in")

    await BOT.change_presence(activity=dc.Activity(type=dc.ActivityType.playing, name="le epic card game"))


cogsLoaded = []

for root, _, files in os.walk("cogs"):
    for filename in files:
        if not filename.endswith('py') or filename.startswith('_'):
            continue
        
        filepath = root.replace("/", ".") + "." + filename[:-3]
        BOT.load_extension(filepath)

        cogsLoaded.append(filename[:-3])

log(f"Loaded cogs: {', '.join(cogsLoaded)}")


try:
    BOT.run(TOKEN)
except dc.LoginFailure:
    print("\nERROR: Invalid bot token! Please open the 'secret.txt' file and paste your bot's token into the file.\n")
    with open("secret.txt", "w") as f:
        f.write("REPLACE_THIS_WITH_YOUR_BOT_TOKEN")
