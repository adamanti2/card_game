import discord as dc
from discord.ext import commands
from lib import error_handling
from lib.cogs import Cog
from lib.game_actions import forfeit
from lib.store import Store


def setup(BOT):
	BOT.add_cog(Forfeit(BOT))


class Forfeit(Cog):

    @commands.slash_command(integration_types={
        dc.IntegrationType.guild_install,
        dc.IntegrationType.user_install,
    }, description="Forfeit your current match.")
    async def forfeit(self, ctx: dc.ApplicationContext):
        user: dc.User = ctx.author # type: ignore
        with await Store.load() as store:
            await forfeit.action(store, user.id, ctx.interaction)
