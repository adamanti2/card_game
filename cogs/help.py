from typing import Dict, List
import discord as dc
from discord.ext import commands
from lib.cogs import Cog
from lib.views.player import PlayerView


def setup(BOT):
	BOT.add_cog(Help(BOT))


class Help(Cog):

    ORDER = (
        "help",
        "challenge",
        "game",
        "forfeit",
        "leave",
    )

    @commands.slash_command(integration_types={
        dc.IntegrationType.guild_install,
        dc.IntegrationType.user_install,
    }, description="Lists available commands for Tonya's Card Game.")
    async def help(self, ctx: dc.ApplicationContext):
        embed = dc.Embed(
            title="Tonya's Card Game",
        )

        commands_unordered: List[dc.SlashCommand] = []
        command_names: Dict[str, dc.SlashCommand] = {}
        for command in self.BOT.commands:
            assert isinstance(command, dc.SlashCommand)
            if command.name in self.ORDER:
                command_names[command.name] = command
            else:
                commands_unordered.append(command)

        commands: List[dc.SlashCommand] = []
        for name in self.ORDER:
            commands.append(command_names[name])
        commands += commands_unordered

        content = ""
        for command in commands:
            content += f"`/{command.name}` : {command.description}\n"
        embed.add_field(name="List of commands", value=content)

        await ctx.respond(
            embed = embed,
            ephemeral = True,
        )
