import discord as dc
from discord.ext import commands
from lib.cogs import Cog
from lib.views.player import PlayerView


def setup(BOT):
	BOT.add_cog(Views(BOT))


class Views(Cog):

    @commands.Cog.listener()
    async def on_ready(self):
        self.BOT.add_view(PlayerView(timeout=None))
