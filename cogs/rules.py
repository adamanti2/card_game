from dataclasses import dataclass
from typing import ClassVar, Dict, Self
import discord as dc
from discord.ext import commands
from lib.cogs import Cog
from lib.cards import Cards


def setup(BOT):
    BOT.add_cog(Rules(BOT))


@dataclass
class Page:
    emoji: str
    title: str
    content: str

    def create_embed(self) -> dc.Embed:
        embed = dc.Embed(title="Tonya's Card Game")
        embed.add_field(name=self.title, value=self.content)
        return embed

def _read_file(path: str) -> str:
    with open(path, 'r') as file:
        return file.read()

def _list_cards(cards_from: int, to: int) -> str:
    content = ""
    for card in Cards.all()[cards_from:to]:
        content += f"{card.emoji} (**{card.value}**) **{card.full_name}** - {card.ability}\n\n"
    return content

PAGES = (
    Page(
        emoji="❓",
        title="Game Rules",
        content=_read_file("important_stuff/rules/rules.md"),
    ),
    Page(
        emoji="🃏",
        title="Cards 1-7",
        content=_list_cards(0, 7),
    ),
    Page(
        emoji="🕯️",
        title="Cards 8-14",
        content=_list_cards(7, 14),
    ),
    Page(
        emoji="👑",
        title="Cards 15-20",
        content=_list_cards(14, 20),
    ),
    Page(
        emoji="🤖",
        title="Using the Bot",
        content=_read_file("important_stuff/rules/bot.md"),
    ),
    Page(
        emoji="❔",
        title="Other Rules",
        content=_read_file("important_stuff/rules/misc_rules.md"),
    ),
)

user_pages: Dict[int, int] = {}

class RulesView(dc.ui.View):

    async def _goto(self, index: int, interaction: dc.Interaction):
        assert interaction.user
        user_pages[interaction.user.id] = index
        view = self.create(index)
        await interaction.edit(embed = PAGES[index].create_embed(), view = view)
    
    async def _step(self, value: int, interaction: dc.Interaction):
        assert interaction.user
        index = user_pages.get(interaction.user.id, 0) + value
        if index not in range(len(PAGES)):
            await interaction.response.defer()
            return
        await self._goto(index, interaction)

    @dc.ui.select(
        placeholder="Select page to read...",
        custom_id="page_select",
        options=[],
    )
    async def select_page(self, select: dc.ui.Select, interaction: dc.Interaction):
        index = int(str(select.values[0]))
        await self._goto(index, interaction)

    @dc.ui.button(
        label="Back",
        emoji="👈",
        style=dc.ButtonStyle.primary,
        custom_id="page_back",
        row=1,
    )
    async def button_page_back(self, _, interaction: dc.Interaction):
        await self._step(-1, interaction)

    @dc.ui.button(
        label="Next",
        emoji="👉",
        style=dc.ButtonStyle.primary,
        custom_id="page_next",
        row=1,
    )
    async def button_page_next(self, _, interaction: dc.Interaction):
        await self._step(1, interaction)
    
    @classmethod
    def create(cls, index: int = 0) -> Self:
        view = cls(timeout=None)

        page_select = view.children[0]
        assert isinstance(page_select, dc.ui.Select)
        for i, page in enumerate(PAGES):
            page_select.options.append(dc.SelectOption(
                emoji=page.emoji,
                label=page.title,
                value=str(i),
                default=False,
            ))
        page_select.options[index].default = True

        if index == 0:
            assert isinstance(view.children[1], dc.ui.Button)
            view.children[1].disabled = True
        elif index == len(PAGES) - 1:
            assert isinstance(view.children[2], dc.ui.Button)
            view.children[2].disabled = True

        return view


class Rules(Cog):

    @commands.Cog.listener()
    async def on_ready(self):
        self.BOT.add_view(RulesView.create())
    
    @commands.slash_command(integration_types={
        dc.IntegrationType.guild_install,
        dc.IntegrationType.user_install,
    }, description="Explains the rules of Tonya's Card Game & the abilities of each card.")
    async def rules(self, ctx: dc.ApplicationContext):
        embed = PAGES[0].create_embed()
        view = RulesView.create()
        await ctx.respond(
            embed = embed,
            view = view,
            ephemeral = True,
        )
