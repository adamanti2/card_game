import discord as dc
from discord.ext import commands
from lib import error_handling
from lib.cogs import Cog
from lib.game import Game, PlayerUI
from lib.store import Store
from lib.utils import find
from lib.views.player import PlayerView


def setup(BOT):
	BOT.add_cog(GameCog(BOT))


async def post_player_message(interaction: dc.Interaction):
    assert interaction.user

    with await Store.load() as store:
        try:
            game, player = store.get_game_and_player(interaction.user.id)
            player_interaction = await interaction.respond(
                content=game.get_player_message(interaction.user.id),
                view=PlayerView.create(game, player),
                ephemeral=True,
            )
            assert isinstance(player_interaction, dc.Interaction)
            player.last_player_ui = PlayerUI(
                player_interaction,
                PlayerView,
            )
        except error_handling.NotInGameError:
            await interaction.respond(content=':x: Could not find this game!', ephemeral=True)


class GameCog(Cog):

    @commands.Cog.listener()
    async def on_ready(self):
        self.BOT.add_view(self.GameInfoView(timeout=None))


    @commands.slash_command(integration_types={
        dc.IntegrationType.guild_install,
        dc.IntegrationType.user_install,
    }, description="Bring up your current game view again.")
    async def game(self, ctx: dc.ApplicationContext):
        await post_player_message(ctx.interaction)


    class GameInfoView(dc.ui.View):
        @dc.ui.button(label="View Cards", style=dc.ButtonStyle.success, emoji="🃏", custom_id="view_game")
        async def button_view(self, button, interaction: dc.Interaction):
            await post_player_message(interaction)
                
        
        @dc.ui.button(label="Refresh", style=dc.ButtonStyle.secondary, emoji="🔄", custom_id="refresh_game")
        async def button_refresh(self, button, interaction: dc.Interaction):
            assert interaction.message
            messageId = interaction.message.id

            with await Store.load() as store:
                game = find(lambda game: game.info_message_id == messageId, store.games)
                if not game:
                    await interaction.response.defer()
                    return
                await interaction.edit(
                    content=game.get_info_message(),
                    view=GameCog.GameInfoView(timeout=None),
                )
                if game.info_message_id:
                    Game.game_interactions[game.info_message_id] = interaction
