import discord as dc
from discord.ext import commands
from lib.cogs import Cog
from lib import error_handling


def setup(BOT):
	BOT.add_cog(ErrorHandling(BOT))


class ErrorHandling(Cog):

    @commands.Cog.listener()
    async def on_application_command_error(self, ctx: dc.ApplicationContext, error: dc.DiscordException):
        PREFIX = ":x: "

        if isinstance(error, commands.errors.NotOwner):
            await ctx.respond(f'{PREFIX}Only an admin can run `{ctx.command.name}`.', ephemeral=True)
        elif isinstance(error, error_handling.NotInGameError):
            await ctx.respond(f'{PREFIX}You are not in a game.', ephemeral=True)
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            await ctx.respond(f'{PREFIX}Missing argument; please specify `{error.param}`.', ephemeral=True)
        elif isinstance(error, commands.CommandInvokeError):
            await ctx.respond(f'{PREFIX}{str(error.original)[:500]}', ephemeral=True)
        elif isinstance(error, commands.CommandNotFound):
            await ctx.respond(f'{PREFIX}Unknown command `{ctx.command.name}`.', ephemeral=True)
        else:
            raise error
