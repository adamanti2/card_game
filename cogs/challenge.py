import discord as dc
from random import random
from dataclasses import dataclass
from typing import Dict
from discord.ext import commands
from .game import GameCog
from lib.cogs import Cog
from lib.store import Store
from lib.game import Game, Player


def setup(BOT):
	BOT.add_cog(ChallengeCog(BOT))


async def not_in_game_check(ctx: dc.ApplicationContext | dc.Interaction, user1Id: int, user2Id: int) -> bool:
    with await Store.load() as store:
        for id, error_message, error_ephemeral in (
            ( user1Id, f":x: You are already in a game! Run `/leave` or press the button to leave your current game.", True ),
            ( user2Id, f":x: <@{user2Id}> is already in a game! They may run `/leave` or press the button to leave their game.", False ),
        ):
            game = store.get_game_by_player(id)
            if game and not game.winner:
                await ctx.respond(
                    content=error_message,
                    ephemeral=error_ephemeral,
                    view=ChallengeCog.LeaveView(),
                    allowed_mentions=dc.AllowedMentions.none(),
                )
                return False
    return True


async def leave_player(user: dc.User, interaction: dc.Interaction):
    with await Store.load() as store:
        game = store.get_game_by_player(user.id)
        if game:
            opponent = game.get_opponent(user.id)
            store.games.remove(game)
            if opponent:
                await interaction.respond(f"You left your game against <@{opponent.id}>. Use `/challenge` to start a new game.", ephemeral=True)
            else:
                await interaction.respond(f"You left your game. Use `/challenge` to start a new game.", ephemeral=True)
            return
    
    challenges = [*filter(lambda id: ChallengeCog.challenges[id].author.id == user.id, ChallengeCog.challenges.keys())]
    if len(challenges) > 0:
        if len(challenges) == 1:
            challenge = ChallengeCog.challenges[challenges[0]]
            del ChallengeCog.challenges[challenges[0]]
            await interaction.respond(f"You cancelled your challenge for <@{challenge.challenger.id}>. Use `/challenge` to create a new one.", allowed_mentions=dc.AllowedMentions.none())
        else:
            for challenge in challenges:
                del ChallengeCog.challenges[challenge]
            await interaction.respond(f"You cancelled your current challenges. Use `/challenge` to create a new one.", allowed_mentions=dc.AllowedMentions.none())
        return

    await interaction.respond(f":x: You are not currently in a game.", ephemeral=True)


@dataclass
class Challenge:
    author: dc.User
    challenger: dc.User


class ChallengeCog(Cog):

    challenges: Dict[int, Challenge] = {}

    @commands.Cog.listener()
    async def on_ready(self):
        self.BOT.add_view(self.ChallengeView(timeout=None))

    @commands.slash_command(integration_types={
        dc.IntegrationType.guild_install,
        dc.IntegrationType.user_install,
    }, description="Challenge a user to a game of Tonya's Card Game.")
    async def challenge(self, ctx: dc.ApplicationContext, player: dc.User):
        author: dc.User = ctx.author # type: ignore
        if author.id == player.id:
            await ctx.respond(f":x: Cannot challenge yourself!")
            return
        
        if not await not_in_game_check(ctx, author.id, player.id):
            return
        
        for challenge in ChallengeCog.challenges.values():
            if challenge.challenger.id == author.id and challenge.author.id == player.id:
                await ctx.respond(f":x: That player has already challenged you!", ephemeral=True)
                return
        
        interaction = await ctx.respond(
            content=f"🃏 <@{player.id}>, you have been **challenged** by <@{author.id}> to a match of **Tonya's Card Game**! 🃏\n### Press the button to accept and start the game.",
            view=ChallengeCog.ChallengeView(timeout=None),
        )
        assert isinstance(interaction, dc.Interaction)
        message = await interaction.original_response()
        ChallengeCog.challenges[message.id] = Challenge(author, player)

    class ChallengeView(dc.ui.View):
        @dc.ui.button(label="Play", style=dc.ButtonStyle.red, emoji="🔥", custom_id="challenge_play")
        async def button_play(self, button, interaction: dc.Interaction):
            assert interaction.message
            assert interaction.user

            if interaction.message.id not in ChallengeCog.challenges:
                await interaction.respond(":x: This challenge has expired. Use `/challenge` to create a new challenge.", ephemeral=True)

            else:
                challenge = ChallengeCog.challenges[interaction.message.id]

                if interaction.user.id != challenge.challenger.id:
                    await interaction.respond(":x: You are not the player who was challenged!", ephemeral=True)
                    return
                
                with await Store.load() as store:
                    for id in (challenge.challenger.id, challenge.author.id):
                        game = store.get_game_by_player(id)
                        if game:
                            if not game.winner:
                                return
                            store.games.remove(game)
                    
                    del ChallengeCog.challenges[interaction.message.id]
                    
                    players = (challenge.author, challenge.challenger)
                    if random() < 0.5:
                        players = (players[1], players[0])

                    game = Game(
                        players=(
                            Player.new(players[0].id, players[0].display_name),
                            Player.new(players[1].id, players[1].display_name),
                        )
                    )
                    store.games.append(game)

                    await interaction.edit(
                        content=game.get_info_message(),
                        view=GameCog.GameInfoView(timeout=None),
                    )

                    game_message = await interaction.original_response()
                    game.info_message_id = game_message.id
                    game.info_channel_id = game_message.channel.id
                    Game.game_interactions[game_message.id] = interaction


    @commands.slash_command(integration_types={
        dc.IntegrationType.guild_install,
        dc.IntegrationType.user_install,
    }, description="Leave your current game.")
    async def leave(self, ctx: dc.ApplicationContext):
        user: dc.User = ctx.author # type: ignore
        await leave_player(user, ctx.interaction)
    
    class LeaveView(dc.ui.View):
        @dc.ui.button(label="Leave Game", style=dc.ButtonStyle.secondary, emoji="❌", custom_id="game_leave")
        async def button_leave(self, _, interaction: dc.Interaction):
            user: dc.User = interaction.user # type: ignore
            await leave_player(user, interaction)
