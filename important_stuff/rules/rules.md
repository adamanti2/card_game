Each player has a deck of 20 unique cards numbered from 1 to 20. They must have 5 of them in hand at any time (unless there are no more to draw).

Each turn, a player picks a card from their hand to play, and they reveal them at the same time. Whoever played the card with a higher numeric value wins a point, then both cards are discarded (typically).

Each card has a special ability that can help each player get the upper hand over their opponent.

You win if either:
1. You earn 9 points.
2. Your opponent runs out of cards.
