The bot automates some actions, such as playing cards. However, many actions must be checked and performed manually.

For example, if a player has less than 5 cards, they must draw some cards themselves. Determining who won each round, as well as scoring points is also done manually.

If a card's ability relies on certain conditions such as winning or losing, players will always have the option to trigger the ability - it's up to them to know whether they should or not.

You may run `/challenge` to challenge another player to a match. During a match, you may run `/game` to bring up your game view again if you need to.
