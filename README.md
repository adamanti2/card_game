Installation:
- Install Python 3.10 or above
- Open command terminal in the bot's folder
- Run `pip install -r requirements.txt`

Running the bot:
- Open command terminal in the bot's folder
- Run `py .`
