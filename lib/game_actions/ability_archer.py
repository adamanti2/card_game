import discord as dc
from random import Random, randint
from lib.cards import Cards
from lib.views.card_select import CardSelectView
from lib.game import Game, Player
from lib.store import Store
from . import play_card


async def action(player: Player, interaction: dc.Interaction):
    async def callback(cardValue: int, interaction: dc.Interaction):
        assert interaction.user
        with await Store.load() as store:
            game, player = store.get_game_and_player(interaction.user.id)
            player.status.archer_shot = True
            await play_card.action(cardValue, game, player, interaction, override_ui=False)
            if player.played_card:
                card = player.played_card
                await interaction.edit(
                    content=f"You played {card.emoji} **{card.full_name}** (**{card.value}**). Waiting for opponent...",
                )

    if not player.status.archer_seed:
        player.status.archer_seed = randint(1, 999999)
    rng = Random(player.status.archer_seed)

    hand = player.hand.copy()
    rng.shuffle(hand)
    hand = hand[:2]
    
    lines = []
    lines.append(f"{Cards.ARCHER.emoji} You must play one of these 2 cards:")
    for i, card in enumerate(hand):
        lines.append(f"{i}. {card.emoji} (**{card.value}**) {card.full_name}")

    view = CardSelectView.create("game_ability_archer", hand, callback)
    view.select.placeholder = f"{Cards.ARCHER.emoji} Choose one of these 2 cards..."
    player.last_player_ui.view_type = CardSelectView
    await interaction.edit(view=view, content="\n".join(lines))
