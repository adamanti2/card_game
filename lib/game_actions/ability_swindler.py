import discord as dc
from lib.cards import Cards
from lib.game_actions import discard
from lib.game import Player


async def action(player: Player, interaction: dc.Interaction):
    lines = []
    if len(player.hand) == 0:
        lines.append(f"({Cards.SWINDLER.emoji}) <@{player.id}>'s hand is **empty**.")
    else:
        lines.append(f"({Cards.SWINDLER.emoji}) <@{player.id}>'s hand:")
        for i, card in enumerate(player.hand):
            lines.append(f"{i}. {card.emoji} (**{card.value}**) {card.full_name}")
    player.status.swindler_reveal = True
    await discard.action(player, interaction)
    await interaction.followup.send(content="\n".join(lines), allowed_mentions=dc.AllowedMentions.none())
