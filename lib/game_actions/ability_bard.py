import discord as dc
from random import choice
from lib.cards import Cards
from lib.const import INFO_DELETE_AFTER
from lib.game import Game, Player
from lib.utils import async_task


async def action(game: Game, player: Player, interaction: dc.Interaction):
    discarded = []
    drew = len(player.deck) != 0

    if len(player.deck) == 0:
        card = choice(player.hand)
        player.discard(card)
        discarded.append(card)
    
    for _ in range(2):
        if len(player.deck) == 0:
            break
        player.draw()
        card = player.hand[-1]
        player.discard(card)
        discarded.append(card)
    text = " and your ".join(map(
        lambda card: f"{card.emoji} **{card.name}**",
        discarded,
    ))

    if not (player.last_played and player.last_played.special_draw):
        player.fill_hand()

    opponent = game.get_opponent(player.id)
    assert opponent
    async_task(game.update_ui(opponent, override=False))
    await game.update_ui(player, interaction)

    await interaction.followup.send(content=f"({Cards.BARD.emoji}) The Bard has discarded your {text}.", allowed_mentions=dc.AllowedMentions.none(), ephemeral=True)
    await interaction.followup.send(content=f"({Cards.BARD.emoji}) <@{player.id}> {"drew and " if drew else ""}discarded {len(discarded)} card{"s" if len(discarded) > 1 else ""}.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)
