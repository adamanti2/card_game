import discord as dc
from lib.views.card_select import CardSelectView
from lib.cards import Card, Cards
from lib.const import INFO_DELETE_AFTER
from lib.store import Store
from lib.game import Game, Player
from . import play_card


async def _message(card: Card, player: Player, interaction: dc.Interaction):
    await interaction.followup.send(f"({Cards.FORTUNE_TELLER.emoji}) <@{player.id}> will play {card.emoji} (**{card.value}**) {card.full_name}.")


async def action(game: Game, player: Player, interaction: dc.Interaction):
    if player.status.fortune_teller_reveal:
        await interaction.response.defer()
        await interaction.followup.send(content=":x: You have already played and revealed a card.", ephemeral=True)
        return
    
    if player.played_card:
        player.status.fortune_teller_reveal = True
        await interaction.response.defer()
        await _message(player.played_card, player, interaction)

    else:
        async def callback(cardValue: int, interaction: dc.Interaction):
            assert interaction.user
            with await Store.load() as store:
                game, player = store.get_game_and_player(interaction.user.id)
                await play_card.action(cardValue, game, player, interaction)

                if player.played_card:
                    player.status.fortune_teller_reveal = True
                    await _message(player.played_card, player, interaction)

        view = CardSelectView.create("game_ability_fortune_teller", player.hand, callback)
        view.select.placeholder = f"{Cards.FORTUNE_TELLER.emoji} Choose a card to play..."
        player.last_player_ui.view_type = CardSelectView
        await interaction.edit(view=view, content=f"Choose a card to play, and to reveal to your opponent.")
