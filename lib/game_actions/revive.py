import discord as dc
from lib.const import INFO_DELETE_AFTER
from lib.views.card_select import CardSelectView
from ..game import Player
from ..store import Store
from ..utils import find, async_task


async def action(player: Player, interaction: dc.Interaction):
    async def callback(cardValue: int, interaction: dc.Interaction):
        assert interaction.user
        with await Store.load() as store:
            game, player = store.get_game_and_player(interaction.user.id)
            card = find(lambda card: card.value == cardValue, player.discarded)
            if not card:
                return
            player.discarded.remove(card)
            player.hand.append(card)
            
            opponent = game.get_opponent(player.id)
            assert opponent
            async_task(game.update_info_message(interaction.client))
            async_task(game.update_ui(opponent, override=False))
            await async_task(game.update_ui(player, interaction))
            await interaction.followup.send(content=f"<@{player.id}> revived a card from their discard pile.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)

    view = CardSelectView.create("game_action_revive", player.discarded, callback)
    view.select.placeholder = "👻 Choose a card to recover..."
    player.last_player_ui.view_type = CardSelectView
    await interaction.edit(view=view, content=f"Choose a card from your discard pile to revive.")
