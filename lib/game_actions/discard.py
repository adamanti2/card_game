import discord as dc
from lib.const import INFO_DELETE_AFTER
from lib.views.card_select import CardSelectView
from lib.game import Game, Player
from lib.cards import Card, Cards
from lib.store import Store
from lib.utils import async_task


async def action(player: Player, interaction: dc.Interaction):
    async def callback(cardValue: int, interaction: dc.Interaction):
        assert interaction.user
        with await Store.load() as store:
            game, player = store.get_game_and_player(interaction.user.id)
            card = Card.from_value(cardValue)
            player.discard(card)
            
            opponent = game.get_opponent(player.id)
            assert opponent
            async_task(game.update_info_message(interaction.client))
            async_task(game.update_ui(opponent, override=False))
            await async_task(game.update_ui(player, interaction))
            
            if player.status.swindler_reveal:
                player.status.swindler_reveal = False
                await interaction.followup.send(content=f"({Cards.SWINDLER.emoji}) <@{player.id}> discarded {card.emoji} (**{card.value}**) {card.full_name}.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)
            else:
                await interaction.followup.send(content=f"<@{player.id}> discarded a card.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)

    view = CardSelectView.create("game_action_discard", player.hand, callback)
    view.select.placeholder = "❎ Choose a card to discard..."
    player.last_player_ui.view_type = CardSelectView
    await interaction.edit(view=view, content=f"Choose a card to discard.")
