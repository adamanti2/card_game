import discord as dc
from lib.cards import Card, Cards
from lib.const import INFO_DELETE_AFTER
from lib.views.card_select import CardSelectView
from lib.game import Game, Player
from lib.store import Store
from lib.utils import async_task


async def action(player: Player, interaction: dc.Interaction):
    async def callback(cardValue: int, interaction: dc.Interaction):
        assert interaction.user
        with await Store.load() as store:
            game, player = store.get_game_and_player(interaction.user.id)
            card = Card.from_value(cardValue)
            player.discard(card)

            opponent = game.get_opponent(player.id)
            assert opponent and opponent.last_played
            if not opponent.last_played.special_draw_opponent:
                player.fill_hand()
                async_task(game.update_ui(opponent, override=False))
            
            value = card.value + Cards.SCOUT.value
            async_task(game.update_info_message(interaction.client))
            await async_task(game.update_ui(player, interaction))
            await interaction.followup.send(f"({Cards.SCOUT.emoji}) <@{player.id}> has added {card.emoji} (**{card.value}**) **{card.name}** to the {Cards.SCOUT.name}, adding up to {value}.", delete_after=INFO_DELETE_AFTER)

    view = CardSelectView.create("game_ability_scout", player.hand, callback)
    view.select.placeholder = f"{Cards.SCOUT.emoji} Choose a card to add..."
    player.last_player_ui.view_type = CardSelectView
    await interaction.edit(view=view, content=f"Choose a card from your hand to add to {Cards.SCOUT.full_name}'s value.")
