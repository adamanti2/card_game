import discord as dc
from lib.const import INFO_DELETE_AFTER
from lib.cards import Card
from lib.game import Game, Player
from lib.utils import async_task


async def recover(card: Card, message: str, game: Game, player: Player, interaction: dc.Interaction):
    assert interaction.user

    if card not in player.discarded:
        return
    
    player.discarded.remove(card)
    player.hand.append(card)
    
    opponent = game.get_opponent(player.id)
    assert opponent

    task = async_task(game.update_ui(player, interaction))
    async_task(game.update_ui(opponent, override=False))
    async_task(game.update_info_message(interaction.client))
    await task
    await interaction.followup.send(content=message, allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)
