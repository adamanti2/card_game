import discord as dc
from lib.store import Store
from lib.utils import async_task


async def action(store: Store, user_id: int, interaction: dc.Interaction):
    game, player = store.get_game_and_player(user_id)
    
    player1, player2 = game.players

    winner = game.winner
    if not winner:
        winner = game.get_opponent(player.id)
        assert winner
    
    lines = []

    lines.append(f"**The game has concluded.** <@{winner.id}> has **won**!")
    lines.append("")

    line_scores = f"The scores are: <@{player1.id}> **{player1.points}** - **{player2.points}** <@{player2.id}>"
    line_cards = f"Cards each player has left: <@{player1.id}> **{len(player1.hand + player1.deck)}** - **{len(player2.hand + player2.deck)}** <@{player2.id}>"
    if len(player1.hand) == 0 or len(player2.hand) == 0:
        lines.append(line_cards)
        lines.append(line_scores)
    else:
        lines.append(line_scores)
        lines.append(line_cards)
    
    lines.append("")
    lines.append(f"Run `/challenge` to start a new game.")

    async_task(interaction.respond(content='\n'.join(lines), allowed_mentions=dc.AllowedMentions.none()))

    store.games.remove(game)
