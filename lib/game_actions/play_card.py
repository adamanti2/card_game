import discord as dc
from typing import Dict
from lib.cards import Cards
from lib.const import INFO_DELETE_AFTER
from lib.views.cards_played import CardsPlayedView
from lib.game import Game, Player
from lib.utils import find, async_task


async def action(card_value, game: Game, player: Player, interaction: dc.Interaction, override_ui: bool = True):
    assert interaction.user
    card_value = str(card_value)

    if player.status.fortune_teller_reveal:
        await interaction.edit()
        await interaction.followup.send(f":x: You can't play a different card after revealing to the Fortune Teller!", ephemeral=True)
        return

    opponent = game.get_opponent(interaction.user.id)
    assert opponent

    if card_value == "card_cancel":
        if player.played_index is None:
            await interaction.edit()
            await interaction.followup.send(f":x: You have not played a card yet!", ephemeral=True)
            return
        
        player.played_index = None
        async_task(game.update_ui(player, interaction))
        async_task(game.update_ui(opponent, override=False))
        return

    card = find(lambda card: str(card.value) == card_value, player.hand)
    if card is None:
        await interaction.edit()
        await interaction.followup.send(f":x: You cannot play card **{card_value}** as you do not have it!", ephemeral=True)
        return

    player.played_index = player.hand.index(card)

    if opponent.played_index is None:
        async_task(game.update_ui(player, interaction, override=override_ui))
        async_task(game.update_ui(opponent, override=False))
    else:
        content = ""
        for player_iter in game.players:
            card = player_iter.played_card
            opponent_iter = game.get_opponent(player_iter.id)
            assert card and opponent_iter

            statuses = []
            if player_iter.status.archer_shot:
                statuses.append(Cards.ARCHER.emoji)
            if player_iter.last_played == Cards.PROFESSOR:
                statuses.append(Cards.PROFESSOR.emoji + " +2?")
            if opponent_iter.last_played == Cards.EAGLE_FLOCK:
                statuses.append(Cards.EAGLE_FLOCK.emoji + " -4?")
            if Cards.CONTRARIAN in opponent_iter.played[-2:]:
                statuses.append(Cards.CONTRARIAN.emoji + " -3?")
            statuses = ''.join(map(lambda emoji: f" ({emoji})", statuses))

            content += f"<@{player_iter.id}> played {card.emoji} **{card.full_name}** (**{card.value}**){statuses}.\n"

        for player_iter in game.players:
            player_iter.play_and_discard()

        for player_iter in game.players:
            opponent_iter = game.get_opponent(player_iter.id)
            assert player_iter.last_played and opponent_iter and opponent_iter.last_played

            if not player_iter.last_played.special_draw and not opponent_iter.last_played.special_draw_opponent:
                player_iter.fill_hand()
            
            player_iter.status.archer_seed = None
            player_iter.status.fortune_teller_reveal = False

        async_task(game.update_info_message(interaction.client))
        async_task(game.update_ui(opponent))
        await game.update_ui(player, interaction)

        assert player.last_played and opponent.last_played
        await interaction.followup.send(content, view=CardsPlayedView.create((player.last_played, opponent.last_played)), delete_after=(INFO_DELETE_AFTER and INFO_DELETE_AFTER + 15))
