import discord as dc
from lib.const import INFO_DELETE_AFTER
from lib.game import Game, Player
from lib.utils import async_task


async def action(value: int, game: Game, player: Player, interaction: dc.Interaction, player_interaction: bool = True):
    opponent = game.get_opponent(player.id)
    assert opponent

    player.points += value

    async_task(game.update_info_message(interaction.client))
    async_task(game.update_ui(opponent, override=False))
    if player_interaction:
        await game.update_ui(player, interaction)
    else:
        async_task(game.update_ui(player))
        await interaction.response.defer()

    message = f"<@{player.id}> "
    if value >= 0:
        message += "scores"
    else:
        message += "lost"
        if abs(value) == 1:
            message += " a point"
    if abs(value) != 1:
        message += f" {abs(value)} points"
    if value > 0:
        message += "!"
    else:
        message += "."
    message += f" Scores: **{player.points}** - **{opponent.points}** <@{opponent.id}>"
    
    await interaction.followup.send(message, allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)
