import discord as dc
from lib.cards import Cards
from lib.game import Game, Player
from .ability_recover import recover


async def action(game: Game, player: Player, interaction: dc.Interaction):
    await recover(
        card=Cards.SACRED_JESTER,
        message=f"<@{player.id}>'s {Cards.SACRED_JESTER.emoji} Sacred Jester stays in their hand.",
        game=game,
        player=player,
        interaction=interaction,
    )
