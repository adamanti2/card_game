import discord as dc
from random import shuffle
from lib.cards import Cards
from lib.const import INFO_DELETE_AFTER
from lib.views.card_select import CardSelectView
from lib.game import Player
from lib.store import Store
from lib.utils import find, async_task


async def action(player: Player, interaction: dc.Interaction):
    async def callback(cardValue: int, interaction: dc.Interaction):
        assert interaction.user
        with await Store.load() as store:
            game, player = store.get_game_and_player(interaction.user.id)
            opponent = game.get_opponent(player.id)
            assert opponent

            card = find(lambda card: card.value == cardValue, player.deck[-3:])
            if card is None:
                return
            
            player.deck.remove(card)
            player.hand.append(card)
            shuffle(player.deck)

            async_task(game.update_ui(player, interaction))
            async_task(game.update_info_message(interaction.client))
            async_task(game.update_ui(opponent, override=False))
            await interaction.response.defer()
            await interaction.followup.send(f"({Cards.INVENTOR.emoji}) <@{player.id}> drew a card.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)

    view = CardSelectView.create("game_ability_inventor", player.deck[-3:], callback)
    view.select.placeholder = f"{Cards.INVENTOR.emoji} Draw one of these 3 cards..."
    player.last_player_ui.view_type = CardSelectView
    await interaction.edit(view=view, content=f"{Cards.INVENTOR.emoji} Choose one of these 3 cards to draw:")
