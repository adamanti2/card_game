import discord as dc
from lib import error_handling
from lib.cards import Cards
from lib.const import INFO_DELETE_AFTER
from lib.store import Store
from lib.utils import async_task
from lib.game import Game, Player
from lib.game_actions import play_card, score_points, discard, revive, forfeit, ability_archer, ability_swindler, ability_fortune_teller, ability_bard, ability_scout, ability_warlord, ability_inventor, ability_sacred_jester


class GameActions:
    FILL = "fill"
    DISCARD = "discard"
    REVIVE = "revive"
    FORFEIT = "forfeit"
    SCORE_POINT = "score_point"
    SUBTRACT_POINT = "subtract_point"

    ABILITY_ARCHER = "ability_archer"
    ABILITY_SWINDLER = "ability_swindler"
    ABILITY_FORTUNE_TELLER = "ability_fortune_teller"
    ABILITY_BARD = "ability_bard"
    ABILITY_SCOUT = "ability_scout"
    ABILITY_INVENTOR = "ability_inventor"
    ABILITY_WARLORD = "ability_warlord"
    ABILITY_SACRED_JESTER = "ability_sacred_jester"


class PlayerView(dc.ui.View):
    @dc.ui.select(
        row = 0,
        placeholder = "🃏 Choose a card to play...",
        options = [],
        custom_id="game_play_card",
    )
    async def card_select(self, select: dc.ui.Select, interaction):
        with await Store.load() as store:
            game, player = store.get_game_and_player(interaction.user.id)
            if player.status.archer_shot:
                player.status.archer_shot = False
            player.last_player_ui.interaction = interaction
            player.last_player_ui.view_type = type(self)
            await play_card.action(select.values[0], game, player, interaction)
    
    @dc.ui.select(
        row = 1,
        placeholder = "🌀 Or select an action to use...",
        options = [],
        custom_id="game_action",
    )
    async def action_select(self, select: dc.ui.Select, interaction: dc.Interaction):
        await _action_callback(select, interaction)
    

    @classmethod
    def create(cls, game: Game, player: Player):
        opponent = game.get_opponent(player.id)
        assert opponent

        view = cls(timeout=None)

        card_select, action_select = view.children
        assert isinstance(card_select, dc.ui.Select)
        assert isinstance(action_select, dc.ui.Select)

        if len(player.hand) == 0:
            card_select.placeholder = "🃏 You have no cards to play."
            card_select.disabled = True
            card_select.options.append(dc.SelectOption(
                label="You cannot play any cards.",
            ))
        
        for card in player.hand:
            card_select.options.append(dc.SelectOption(
                label=f"({card.value}) {card.full_name}",
                description=card.ability_shortened,
                emoji=card.emoji,
                value=str(card.value),
            ))
        
        if player.played_index is not None:
            card_select.options.insert(0, dc.SelectOption(
                label=f"Cancel card",
                description=f"Retract your {player.played_card.name if player.played_card else "card"}.",
                emoji="✖️",
                value="card_cancel",
            ))
        
        if card_select.disabled:
            action_select.placeholder = "🌀 Select an action to use..."
        action_select.options = [
            dc.SelectOption(
                label="Discard a card",
                emoji="🚫",
                value=GameActions.DISCARD,
            ),
            dc.SelectOption(
                label="Revive a discarded card",
                emoji="👻",
                value=GameActions.REVIVE,
            ),
            dc.SelectOption(
                label="Score 1 point!",
                emoji="✅",
                value=GameActions.SCORE_POINT,
            ),
            dc.SelectOption(
                label="Subtract 1 point",
                emoji="❎",
                value=GameActions.SUBTRACT_POINT,
            ),
        ]
        if player.to_draw > 0:
            action_select.options.insert(0, dc.SelectOption(
                label="Draw a card" if player.to_draw == 1 else f"Draw {player.to_draw} cards",
                emoji="🃏",
                value=GameActions.FILL,
            ))
        
        button_actions = set()
        def add_button(label: str, emoji: str, style: dc.ButtonStyle, id: str):
            while id in button_actions:
                id += "_"
            button_actions.add(id)
            button = dc.ui.Button(
                label=label,
                emoji=emoji,
                style=style,
                row=2,
                custom_id="action_" + id,
            )
            button.callback = lambda interaction: _action_callback(button, interaction)
            view.add_item(button)

        if opponent is game.winner:
            add_button(
                "Forfeit?",
                "🏳️",
                dc.ButtonStyle.red,
                GameActions.FORFEIT,
            )
        if opponent.last_played == Cards.ARCHER:
            add_button(
                "Play one of 2 cards? (Archer)",
                Cards.ARCHER.emoji,
                dc.ButtonStyle.red,
                GameActions.ABILITY_ARCHER,
            )
        elif opponent.last_played == Cards.SWINDLER:
            add_button(
                "Reveal hand to opponent? (Swindler)",
                Cards.SWINDLER.emoji,
                dc.ButtonStyle.red,
                GameActions.ABILITY_SWINDLER,
            )
        elif opponent.last_played == Cards.FORTUNE_TELLER:
            add_button(
                "Show card to opponent? (FT)",
                Cards.FORTUNE_TELLER.emoji,
                dc.ButtonStyle.red,
                GameActions.ABILITY_FORTUNE_TELLER,
            )
        elif opponent.last_played == Cards.BARD:
            add_button(
                "Discard 2 drawn cards? (Bard)",
                Cards.BARD.emoji,
                dc.ButtonStyle.red,
                GameActions.ABILITY_BARD,
            )
        elif opponent.last_played == Cards.POPE:
            add_button(
                "Discard a card? (Pope)",
                Cards.POPE.emoji,
                dc.ButtonStyle.red,
                GameActions.DISCARD,
            )
        if player.last_played == Cards.INVENTOR:
            add_button(
                "Draw one of 3 cards? (Inventor)",
                Cards.INVENTOR.emoji,
                dc.ButtonStyle.success,
                GameActions.ABILITY_INVENTOR,
            )
        elif player.last_played == Cards.SCOUT:
            add_button(
                "Add in a card? (Scout)",
                Cards.SCOUT.emoji,
                dc.ButtonStyle.success,
                GameActions.ABILITY_SCOUT,
            )
        elif player.last_played == Cards.WARLORD:
            add_button(
                "Keep the Warlord?",
                Cards.WARLORD.emoji,
                dc.ButtonStyle.secondary,
                GameActions.ABILITY_WARLORD,
            )
        elif player.last_played == Cards.SACRED_JESTER:
            add_button(
                "Keep the Sacred Jester?",
                Cards.SACRED_JESTER.emoji,
                dc.ButtonStyle.secondary,
                GameActions.ABILITY_SACRED_JESTER,
            )
        elif player.last_played == Cards.NECROMANCER:
            add_button(
                "Revive a card? (Necromancer)",
                Cards.NECROMANCER.emoji,
                dc.ButtonStyle.success,
                GameActions.REVIVE,
            )
        elif player.last_played == Cards.QUEEN:
            add_button(
                "Discard a card? (Queen)",
                Cards.QUEEN.emoji,
                dc.ButtonStyle.red,
                GameActions.DISCARD,
            )
        
        return view


async def _action_callback(item: dc.ui.Select | dc.ui.Button, interaction: dc.Interaction):
    assert interaction.user
    if isinstance(item, dc.ui.Select):
        action = str(item.values[0])
    elif isinstance(item, dc.ui.Button):
        action = str(item.custom_id).rstrip('_')

    if action.startswith('action_'):
        action = action[len('action_'):]
    
    with await Store.load() as store:
        game = store.get_game_by_player(interaction.user.id)
        if game is None:
            await interaction.response.defer()
            await interaction.followup.send(":x: Can't play a card: you are not in a game!", ephemeral=True)
            return
        player = game.get_player(interaction.user.id)
        opponent = game.get_opponent(interaction.user.id)
        assert player and opponent

        player.last_player_ui.interaction = interaction
        player.last_player_ui.view_type = type(item.view) # type: ignore

        if action == GameActions.FILL:
            to_draw = player.to_draw
            player.fill_hand()
            task = async_task(game.update_ui(player, interaction))
            async_task(game.update_ui(opponent, override=False))
            async_task(game.update_info_message(interaction.client))
            await task
            if to_draw == 1:
                await interaction.followup.send(f"<@{player.id}> drew a card.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)
            else:
                await interaction.followup.send(f"<@{player.id}> drew {to_draw} cards.", allowed_mentions=dc.AllowedMentions.none(), delete_after=INFO_DELETE_AFTER)

        elif action == GameActions.DISCARD:
            await discard.action(player, interaction)
        elif action == GameActions.REVIVE:
            await revive.action(player, interaction)
        elif action == GameActions.FORFEIT:
            await forfeit.action(store, player.id, interaction)
        elif action == GameActions.SCORE_POINT:
            await score_points.action(1, game, player, interaction)
        elif action == GameActions.SUBTRACT_POINT:
            await score_points.action(-1, game, player, interaction)
        elif action == GameActions.ABILITY_ARCHER:
            await ability_archer.action(player, interaction)
        elif action == GameActions.ABILITY_SWINDLER:
            await ability_swindler.action(player, interaction)
        elif action == GameActions.ABILITY_FORTUNE_TELLER:
            await ability_fortune_teller.action(game, player, interaction)
        elif action == GameActions.ABILITY_BARD:
            await ability_bard.action(game, player, interaction)
        elif action == GameActions.ABILITY_SCOUT:
            await ability_scout.action(player, interaction)
        elif action == GameActions.ABILITY_INVENTOR:
            await ability_inventor.action(player, interaction)
        elif action == GameActions.ABILITY_WARLORD:
            await ability_warlord.action(game, player, interaction)
        elif action == GameActions.ABILITY_SACRED_JESTER:
            await ability_sacred_jester.action(game, player, interaction)

        else:
            await interaction.response.defer()
            await interaction.followup.send(f":x: Failed to use action: unknown action `{action}`", ephemeral=True)
