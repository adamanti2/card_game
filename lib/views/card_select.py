import discord as dc
from typing import Awaitable, Callable, List
from lib.store import Store
from lib.cards import Card


CardSelectCallback = Callable[[int, dc.Interaction], Awaitable]


class CardSelectView(dc.ui.View):
    callback: CardSelectCallback
    
    @dc.ui.select(
        placeholder = "🃏 Choose a card...",
        options = [],
    )
    async def discard_select(self, select: dc.ui.Select, interaction: dc.Interaction):
        card = select.values[0]
        assert isinstance(card, str)

        if card == "cancel":
            assert interaction.user
            with await Store.load() as store:
                game, player = store.get_game_and_player(interaction.user.id)
                await game.update_ui(player, interaction, override=True)
            return
        
        await self.callback(int(card), interaction)

    @property
    def select(self) -> dc.ui.Select:
        assert isinstance(self.children[0], dc.ui.Select)
        return self.children[0]

    @classmethod
    def create(cls, id: str, cards: List[Card], callback: CardSelectCallback, allow_cancel: bool = True):
        view = cls(timeout=None)
        view.callback = callback

        if len(cards) == 0:
            raise ValueError(f"You have no cards to select.")
        
        for card in cards:
            view.select.options.append(dc.SelectOption(
                label=f"({card.value}) {card.full_name}",
                description=card.ability_shortened,
                emoji=card.emoji,
                value=str(card.value),
            ))
        
        if len(view.select.options) > 25:
            raise ValueError("May not display more than 25 card options")
        
        if allow_cancel:
            view.select.options.insert(0, dc.SelectOption(
                label=f"Cancel selection",
                emoji="✖️",
                value="cancel",
            ))
        
        view.select.custom_id = id
        
        return view
