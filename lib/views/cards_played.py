import discord as dc
from typing import Tuple
from lib.store import Store
from lib.cards import Card, Cards
from lib.game_actions import score_points


async def _button_score_callback(value: int, interaction: dc.Interaction):
    assert interaction.user
    with await Store.load() as store:
        game, player = store.get_game_and_player(interaction.user.id)
        await score_points.action(value, game, player, interaction, player_interaction=False)


class CardsPlayedView(dc.ui.View):
    _INFO_BUTTON_ID_PREFIX = "cards_info_"

    @dc.ui.button(label="+1 point", style=dc.ButtonStyle.secondary, emoji="✅", custom_id="cards_score")
    async def button_score(self, _: dc.ui.Button, interaction: dc.Interaction):
        await _button_score_callback(1, interaction)

    @dc.ui.button(label="Info", style=dc.ButtonStyle.secondary, emoji="❔", custom_id="cards_info_unknown")
    async def button_info(self, button: dc.ui.Button, interaction: dc.Interaction):
        await interaction.response.defer()
        if not button.custom_id or button.custom_id == "cards_info_unknown":
            return
        
        card_values = button.custom_id[len(self._INFO_BUTTON_ID_PREFIX):].split("_")
        cards = map(lambda value: Card.from_value(int(value)), card_values)
        lines = map(lambda card: f"- {card.emoji} (**{card.value}**) **{card.full_name}** - {card.ability.replace('\n', '\n ')}", cards)
        await interaction.followup.send(
            content="\n".join(lines),
            ephemeral=True,
        )
    
    @property
    def info_button(self) -> dc.ui.Button:
        assert isinstance(self.children[-1], dc.ui.Button)
        return self.children[-1]

    @classmethod
    def create(cls, cards: Tuple[Card, Card]):
        view = cls(timeout=None)

        view.info_button.custom_id = f"{cls._INFO_BUTTON_ID_PREFIX}{cards[0].value}_{cards[1].value}"

        if Cards.DRAGON_SLAYER in cards:
            button = dc.ui.Button(
                label="+2 points",
                emoji=Cards.DRAGON_SLAYER.emoji,
                style=dc.ButtonStyle.secondary,
                custom_id="cards_score_2",
            )
            button.callback = lambda interaction: _button_score_callback(2, interaction)
            view.children.insert(1, button)

        return view
