from ._cards import POINTS_TO_WIN, Card, UnknownCard, _Cards


class Cards(_Cards):
    SACRED_JESTER = Card(
        value=1,
        emoji="🃏",
        name="Sacred Jester",
        summary="Disables opponent abilities. Opponent can choose to lose or win. If they win, you keep this card.",
        ability="Any ability of your opponent's card doesn't work. Your opponent has a choice:\n1. You win the round. Discard the Sacred Jester.\n2. Your opponent wins the round. Return the Sacred Jester to your hand.",
        special_draw=True,
    )
    EAGLE_FLOCK = Card(
        value=2,
        emoji="🪽",
        name="Eagle Flock",
        ability="The next card your opponent plays has -4 in value.",
    )
    SCOUT = Card(
        value=3,
        emoji="🥾",
        name="Scout",
        summary="Once both players play their cards, add another card's value to the Scout's value (then discard it).",
        ability="After you and your opponent show your cards, you must choose another card from your hand. The value of that card and The Scout are summed, but it's ability doesn't work.",
        special_draw=True,
    )
    FORTUNE_TELLER = Card(
        value=4,
        emoji="🔮",
        name="Fortune Teller",
        ability="Next round, your opponent plays their card before you.",
    )
    SWINDLER = Card(
        value=5,
        emoji="💰",
        name="Swindler",
        ability="Your opponent shows you their hand. You choose one of their cards to discard.",
    )
    BARD = Card(
        value=6,
        emoji="🎻",
        name="Bard",
        ability="If you lose the round, the next two cards in your opponent's deck get discarded. (If it's empty, they discard 1 random card)",
        special_draw_opponent=True,
    )
    ARCHER = Card(
        value=7,
        emoji="🏹",
        name="Archer",
        ability="Your opponent randomly selects 2 cards from their hand. Next round, they may only play these cards.",
    )
    HERMIT = Card(
        value=8,
        emoji="🕯️",
        name="Hermit",
        ability="The card with the lowest value wins this round.",
    )
    NECROMANCER = Card(
        value=9,
        emoji="☠️",
        name="Necromancer",
        ability="If you win the round, choose a dead character to return to your hand.",
        special_draw=True,
    )
    CONTRARIAN = Card(
        value=10,
        emoji="☯️",
        name="Contrarian",
        summary="If your opponent plays a lower card, you tie. If you lose, their next 2 cards have -3 in value.",
        ability="If your opponents card is of lower or equal value to the Contrarian, the round ties. If it is higher, your opponents next two cards are -3 in value.",
    )
    BLACKSMITH = Card(
        value=11,
        emoji="⚒️",
        name="Blacksmith",
        summary="Your opponent may choose to give you one of their victory points to revive a discarded card.",
        ability="Your opponent has a choice:\n1. Give away one of their victory points. They choose a dead character to shuffle into their deck.\n2. Do nothing.",
    )
    PROFESSOR = Card(
        value=12,
        emoji="🎓",
        name="Professor",
        ability="The next card you play has +2 in value.",
    )
    DRAGON_SLAYER = Card(
        value=13,
        emoji="🐉",
        name="Dragon Slayer",
        ability="The winner of the round gets 2 victory points.",
    )
    WARLORD = Card(
        value=14,
        emoji="⚔️",
        name="Warlord",
        ability="If you win the round, the warlord remains in your hand.",
        special_draw=True,
    )
    INVENTOR = Card(
        value=15,
        emoji="💡",
        name="Inventor",
        summary="Instead of drawing your next card, pick 1 of 3 random cards to draw.",
        ability="Instead of drawing your next card, look at the three top cards in your deck. Pick one of them (then shuffle the deck).",
        special_draw=True,
    )
    ROYAL_GUARD = Card(
        value=16,
        emoji="🛡️",
        name="Royal Guard",
        ability="No abilities (except The Sacred Jester) work this round. Only values are compared.",
    )
    POPE = Card(
        value=17,
        emoji="⛪",
        name="Pope",
        ability="Your opponent chooses one card they have in hand to discard.",
    )
    JUDGE = Card(
        value=18,
        emoji="⚖️",
        name="Judge",
        summary="You may choose to let your opponent win, but disable their card's ability.",
        ability="You have a choice:\n1. Your opponent wins the round. The ability their card has doesn't work.\n2. Do nothing.",
    )
    QUEEN = Card(
        value=19,
        emoji="💎",
        name="Queen",
        ability="If you win the round, choose a card in your hand to discard.",
        special_draw=True,
    )
    KING = Card(
        value=20,
        emoji="👑",
        name="King",
        ability="No ability.",
    )
