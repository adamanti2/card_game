from datetime import datetime

def log(s: str):
    print(f'[{datetime.now()}] {s}')
