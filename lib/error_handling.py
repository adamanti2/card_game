import discord as dc
from discord.ext import commands

class NotInGameError(commands.CommandInvokeError):
    pass
