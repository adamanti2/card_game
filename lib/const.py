import discord as dc
from os.path import isfile

PREFIX = "-"

if isfile("secret.txt"):
    with open("secret.txt", "r") as f:
        TOKEN = f.read().strip()
else:
    with open("secret.txt", "w") as f:
        TOKEN = input("Enter bot token: ").strip()
        f.write(TOKEN)
INTENTS = dc.Intents.all()

BOT = dc.Bot(intents=INTENTS)

INFO_DELETE_AFTER: float = 45 # type: ignore # May be set to None; Pycord requires 'float' type hint
