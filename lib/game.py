import discord as dc
from pydantic import BaseModel, Field, model_serializer
from dataclasses import dataclass
from random import shuffle
from typing import Dict, List, Optional, Self, Tuple, ClassVar
from .cards import Card, Cards, POINTS_TO_WIN
from .utils import find


def new_filled_deck() -> List[Card]:
    cards = Cards.all().copy()
    return cards

def new_shuffled_deck() -> List[Card]:
    deck = new_filled_deck()
    shuffle(deck)
    return deck


class PlayerStatus(BaseModel):
    archer_shot: bool = False
    archer_seed: int | None = None
    swindler_reveal: bool = False
    fortune_teller_reveal: bool = False

    @model_serializer(mode='wrap')
    def serialize(self, handler):
        serialized = handler(self)
        for key in ("archer_shot", "archer_seed", "swindler_reveal", "fortune_teller_reveal"):
            if not serialized[key]:
                del serialized[key]
        return serialized


@dataclass
class PlayerUI:
    interaction: dc.Interaction | None = None
    view_type: type[dc.ui.View] | None = None


class Player(BaseModel):
    id: int
    name: str
    points: int = 0
    status: PlayerStatus = PlayerStatus()
    
    played_index: Optional[int] = None
    hand: List[Card] = []
    deck: List[Card] = Field(default_factory=new_shuffled_deck)

    discarded: List[Card] = []
    played: List[Card] = []

    _last_player_uis: ClassVar[Dict[int, PlayerUI]] = {}

    @property
    def last_player_ui(self) -> PlayerUI:
        if self.id not in Player._last_player_uis:
            Player._last_player_uis[self.id] = PlayerUI()
        return Player._last_player_uis[self.id]

    @last_player_ui.setter
    def last_player_ui(self, value: PlayerUI):
        Player._last_player_uis[self.id] = value
    
    @property
    def played_card(self) -> Optional[Card]:
        if self.played_index is None:
            return None
        return self.hand[self.played_index]
    
    @property
    def last_played(self) -> Optional[Card]:
        if len(self.played) < 1:
            return None
        return self.played[-1]
    
    @property
    def to_draw(self) -> int:
        to_draw = 5 - len(self.hand)
        return min(to_draw, len(self.deck))
    
    @classmethod
    def new(cls, id: int, name: str) -> Self:
        player = cls(id=id, name=name)
        player.fill_hand()
        return player
    
    def draw(self, amount: int = 1):
        for _ in range(amount):
            if len(self.deck) == 0:
                break
            self.hand.append(self.deck.pop())
    
    def fill_hand(self):
        if self.to_draw < 1:
            return
        self.draw(self.to_draw)
    
    def discard(self, card: Card):
        try:
            self.hand.remove(card)
        except ValueError:
            return
        self.discarded.append(card)
    
    def discard_fill(self, card: Card):
        self.discard(card)
        self.fill_hand()
    
    def play_and_discard(self):
        if not self.played_card:
            return
        self.played.append(self.played_card)
        self.discard(self.played_card)
        self.played_index = None


class Game(BaseModel):
    players: Tuple[Player, Player]
    info_message_id: int | None = None
    info_channel_id: int | None = None

    @property
    def winner(self) -> Player | None:
        have_cards = []
        for player in self.players:
            if player.points >= POINTS_TO_WIN:
                return player
            if len(player.hand) + len(player.deck) > 0:
                have_cards.append(player)
        if len(have_cards) == 1:
            return have_cards[0]
        return None

    def get_player(self, user_id: int) -> Player | None:
        return find(lambda player: player.id == user_id, self.players)

    def get_opponent(self, user_id: int) -> Player | None:
        if not self.get_player(user_id):
            return None
        return find(lambda player: player.id != user_id, self.players)


    def get_info_message(self):
        lines = []
        lines.append(f"**Card Game** - <@{self.players[0].id}> vs <@{self.players[1].id}>")
        lines.append(f"Points: **{self.players[0].points}** - **{self.players[1].points}**")
        lines.append("")
        for player in self.players:
            lines.append(f"**{player.name}** holds **{len(player.hand)}** cards. There are **{len(player.deck)}** cards left in their deck.")
        lines.append("")
        lines.append("Click the button to view your cards.")
        return "\n".join(lines)
    
    def get_player_message(self, playerId):
        player = self.get_player(playerId)
        if player is None:
            return ":x: You are not part of this game! Use `/challenge` to create a new game."
        opponent = self.get_opponent(playerId)
        assert opponent

        lines = []
        if len(player.hand) == 0:
            lines.append(f"You have **no more cards** in your hand.")
        elif player.played_card is not None:
            card = player.played_card
            lines.append(f"You played {card.emoji} **{card.full_name}** (**{card.value}**). Waiting for opponent...",)
        else:
            lines.append(f"**Your Hand:**")
            for i, card in enumerate(player.hand):
                lines.append(f"{i}. {card.emoji} (**{card.value}**) {card.full_name}")
        
        lines.append("")
        lines.append(f"Your deck has **{len(player.deck)}** cards left; your opponent has **{len(opponent.deck)}**. They are holding **{len(opponent.hand)}** cards.")
        lines.append(f"Scores: <@{player.id}> **{player.points}** - **{opponent.points}** <@{opponent.id}>")
        if opponent.played_card:
            lines.append("❗ Your opponent has **played their card**. ❗")

        return "\n".join(lines)
    

    game_interactions: ClassVar[Dict[int, dc.Interaction]] = {}

    async def update_info_message(self, bot: dc.Client):
        try:
            await self._edit_info_message(bot)
            return
        except (AssertionError, dc.HTTPException):
            pass
        
        try:
            await self._edit_info_interaction()
            return
        except (AssertionError, dc.HTTPException):
            pass
    
    async def _edit_info_message(self, bot: dc.Client):
        assert self.info_channel_id
        assert self.info_message_id

        channel = bot.get_channel(self.info_channel_id)
        assert isinstance(channel, dc.Thread) or isinstance(channel, dc.TextChannel) or isinstance(channel, dc.VoiceChannel) or isinstance(channel, dc.ForumChannel) or isinstance(channel, dc.StageChannel)

        message = channel.get_partial_message(self.info_message_id)
        await message.edit(content=self.get_info_message())
    
    async def _edit_info_interaction(self):
        assert self.info_message_id
        assert self.info_message_id in Game.game_interactions
        interaction = Game.game_interactions[self.info_message_id]
        await interaction.edit(content=self.get_info_message())
    
    async def update_ui(self, player: Player, interaction: dc.Interaction | None = None, override: bool = True):
        from lib.views.player import PlayerView

        if not interaction:
            interaction = player.last_player_ui.interaction
        else:
            player.last_player_ui.interaction = interaction
        
        if not interaction:
            return
        if not override and player.last_player_ui.view_type is not PlayerView:
            return
        
        player.last_player_ui.view_type = PlayerView
        await interaction.edit(
            content=self.get_player_message(player.id),
            view=PlayerView.create(self, player),
        )
