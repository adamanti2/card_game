import asyncio
from typing import Any, Callable, Coroutine, Iterable, TypeVar


_T = TypeVar("_T")

def find(predicate: Callable[[_T], Any], iterable: Iterable[_T]) -> _T | None:
    for value in iterable:
        if predicate(value):
            return value
    return None

def async_task(coroutine: Coroutine):
    return asyncio.get_event_loop().create_task(coroutine)
