from pydantic import BaseModel, model_serializer, model_validator, field_validator
from typing import List, Self


POINTS_TO_WIN = 9


class UnknownCard(Exception):
    pass


class Card(BaseModel):
    value: int
    emoji: str
    name: str
    ability: str
    summary: str | None = None

    special_draw: bool = False
    special_draw_opponent: bool = False

    @property
    def full_name(self) -> str:
        return f"The {self.name}"
    
    @property
    def ability_shortened(self) -> str:
        ability = self.summary or self.ability
        if len(ability) > 99:
            ability = ability[:99] + "…"
        return ability

    @classmethod
    def from_value(cls, value: int) -> Self:
        from lib.cards import Cards
        for card in Cards.all():
            if card.value != value:
                continue
            return card # type: ignore
        raise UnknownCard(f"Card with value '{value}' does not exist")

    def __eq__(self, o) -> bool:
        if isinstance(o, Card):
            return self.value == o.value
        return False

    @field_validator('summary')
    @classmethod
    def summary_validator(cls, value: str | None) -> str | None:
        if value and len(value) > 100:
            raise ValueError("Card summary must be no longer than 100 characters")
        return value

    @model_serializer()
    def serializer(self) -> int:
        return self.value

    @model_validator(mode='wrap')
    @classmethod
    def validator(cls, data: int | dict, handler, info) -> Self:
        if isinstance(data, int):
            return cls.from_value(data)
        return handler(data)


class _Cards:
    @classmethod
    def all(cls) -> List[Card]:
        cards = []
        for key, value in cls.__dict__.items():
            if key.startswith('__') or callable(value) or type(value) == classmethod:
                continue
            cards.append(value)
        return cards
