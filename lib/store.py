import asyncio
from pydantic import BaseModel
from typing import ClassVar, List, Tuple
from .error_handling import NotInGameError
from .game import Game, Player


class Store(BaseModel):
    games: List[Game]

    def get_game_and_player(self, playerId: int, otherPlayerId: int | None = None) -> Tuple[Game, Player]:
        for game in self.games:
            for player in game.players:
                if player.id == playerId and (otherPlayerId is None or self.get_game_by_player(otherPlayerId) is game):
                    return (game, player)
        raise NotInGameError(f"Player <@{playerId}> is not in a game")

    def get_game_by_player(self, playerId: int, otherPlayerId: int | None = None) -> Game | None:
        try:
            game, player = self.get_game_and_player(playerId, otherPlayerId)
            return game
        except NotInGameError:
            return None

    lock: ClassVar[asyncio.Event | None] = None

    @classmethod
    async def load(cls):
        if cls.lock:
            await cls.lock.wait()
        cls.lock = asyncio.Event()
        
        try:
            with open("store.json", 'r') as f:
                return cls.model_validate_json(f.read())
        except:
            return cls(games=[])

    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, exc_traceback) -> bool:
        cls = type(self)
        if cls.lock:
            cls.lock.set()
        
        with open("store.json", 'w') as f:
            content = self.model_dump_json(indent=2)
            f.write(content)
        if exc_type is not None:
            return False
        return True
